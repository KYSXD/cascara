<script setup lang="ts">
import { computed, ref, watch } from 'vue';
import { GroupRepository } from '@/stores/group.repo';
import { PermissionRepository } from '@/stores/permission.repo';
import { UserRepository } from '@/stores/user.repo';
import { getGroups } from '@/services/group.service';
import { useRepo } from 'pinia-orm';

const props = defineProps({
  username: {
    default: null,
    required: true,
    type: String,
  },
});

const groupOptions = ref([]);
const userGroupsForm = ref([]);
const onGroupSearch = (searchQuery) => {
  getGroups({ searchQuery }).then(({ data }) => {
    groupOptions.value = data.items.map((item) => ({ codename: item.codename, name: item.name }));
  });
};
const onUserGroupsReset = () => {
  userGroupsForm.value = useRepo(UserRepository)
    .where('username', props.username)
    .with('groups')
    .first()
    .groups.map((item) => ({ codename: item.codename, name: item.name }));
};

const permissionOptions = computed(() =>
  useRepo(PermissionRepository)
    .orderBy((permission) => permission.name.toLowerCase(), 'asc')
    .get()
    .map((item) => ({ codename: item.codename, name: item.name })),
);
const userPermissionsForm = ref([]);
const onUserPermissionsReset = () => {
  userPermissionsForm.value = useRepo(UserRepository)
    .where('username', props.username)
    .with('permissions')
    .first()
    .permissions.map((item) => ({ codename: item.codename, name: item.name }));
};

const userForm = ref({
  email: '',
  fullname: '',
  isActive: false,
  isAdmin: false,
  isStaff: false,
  isSuperuser: false,
  username: '',
});

const onUserReset = () => {
  const user = useRepo(UserRepository).where('username', props.username).with('groups').first();
  userForm.value.email = user.email;
  userForm.value.fullname = user.fullname;
  userForm.value.isActive = user.isActive;
  userForm.value.isAdmin = user.isAdmin;
  userForm.value.isStaff = user.isStaff;
  userForm.value.isSuperuser = user.isSuperuser;
  userForm.value.username = user.username;
};

const onUserSubmit = (data) => {
  useRepo(UserRepository)
    .setUser(props.username, data)
    .then(() => {
      useRepo(UserRepository)
        .fetchUser(props.username)
        .then(() => onUserReset());
    });
};

const onUserGroupsSubmit = (data) => {
  useRepo(UserRepository)
    .setUserGroups(props.username, data)
    .then(() => {
      useRepo(GroupRepository)
        .fetchUserGroups(props.username, { limit: 1000 })
        .then(() => onUserGroupsReset());
    });
};

const onUserPermissionsSubmit = (data) => {
  useRepo(UserRepository)
    .setUserPermissions(props.username, data)
    .then(() => {
      useRepo(PermissionRepository)
        .fetchUserPermissions(props.username, { limit: 1000 })
        .then(() => onUserPermissionsReset());
    });
};

watch(
  () => props.username,
  (newValue) => {
    Promise.all([
      useRepo(UserRepository).fetchUser(newValue),
      useRepo(PermissionRepository).fetchAll({ limit: 1000 }),
      useRepo(GroupRepository).fetchUserGroups(newValue, { limit: 1000 }),
      useRepo(PermissionRepository).fetchUserPermissions(newValue, { limit: 1000 }),
    ]).then(() => {
      onUserReset();
      onUserGroupsReset();
      onUserPermissionsReset();
    });
  },
  { immediate: true },
);
</script>

<template>
  <UserEditForm v-model="userForm" @on-submit="onUserSubmit(userForm)" @on-reset="onUserReset" />

  <form @submit.prevent="onUserGroupsSubmit(userGroupsForm)" @reset.prevent="onUserGroupsReset">
    <label class="form-check-label" for="user-groups">Groups</label>
    <VueMultiselect
      id="user-groups"
      v-model="userGroupsForm"
      :options="groupOptions"
      :multiple="true"
      :searchable="true"
      placeholder="Start typing"
      label="name"
      track-by="codename"
      class="mb-3"
      @search-change="onGroupSearch"
    >
      <template #selection="{ values, isOpen }">
        <span v-if="values.length" v-show="!isOpen" class="multiselect__single">
          <span
            v-for="item in values"
            :key="item.codename"
            class="badge rounded-pill text-bg-primary"
            >{{ item.name }}</span
          >
        </span>
      </template>
    </VueMultiselect>

    <div class="col-12 text-end">
      <button type="reset" class="btn btn-secondary me-1">
        {{ $t('commons.form.reset') }}
      </button>
      <button type="submit" class="btn btn-primary">
        {{ $t('commons.form.submit') }}
      </button>
    </div>
  </form>

  <form
    @submit.prevent="onUserPermissionsSubmit(userPermissionsForm)"
    @reset.prevent="onUserPermissionsReset"
  >
    <legend>Permissions</legend>

    <ul class="list-group mb-3">
      <li
        v-for="permission in permissionOptions"
        :key="permission.codename"
        class="list-group-item"
      >
        <div class="form-check form-switch">
          <input
            :id="permission.codename"
            v-model="userPermissionsForm"
            class="form-check-input me-1"
            type="checkbox"
            :value="permission"
            role="switch"
          />
          <label class="form-check-label stretched-link" :for="permission.codename">{{
            permission.name
          }}</label>
        </div>
      </li>
    </ul>

    <div class="col-12 text-end">
      <button type="reset" class="btn btn-secondary me-1">
        {{ $t('commons.form.reset') }}
      </button>
      <button type="submit" class="btn btn-primary">
        {{ $t('commons.form.submit') }}
      </button>
    </div>
  </form>
</template>
