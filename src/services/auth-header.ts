export const authHeader = () => {
  const user = JSON.parse(localStorage.getItem('user') || 'null');

  if (user) {
    const raw = `${user.username}:${user.token}`;
    const auth = btoa(raw);

    return { Authorization: `Basic ${auth}` };
  }
  return {};
};
