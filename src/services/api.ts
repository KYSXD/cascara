import { authHeader } from './auth-header';
import axios from 'axios';
import { useAlertStore } from '@/stores/alert.store';
import { useAuthStore } from '@/stores/auth.store';

const API_URL = import.meta.env.VITE_PVM_API_URL;

const api = axios.create({
  baseURL: API_URL,
});

api.interceptors.request.use((config) => {
  const { Authorization } = authHeader();
  config.headers.Authorization = Authorization;

  return config;
});

api.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    const alertStore = useAlertStore();
    const statusCodeUnauthorized = 401;
    const statusCodeForbidden = 403;
    const statusNotFound = 404;
    if (error.request.status === statusCodeUnauthorized) {
      useAuthStore().logout();
      alertStore.error('request.unauthorized');
    } else if (error.request.status === statusCodeForbidden) {
      alertStore.error('request.forbidden');
    } else if (error.request.status === statusNotFound) {
      alertStore.error('request.notFound');
    } else {
      alertStore.error('request.error');
    }
  },
);

export default api;
