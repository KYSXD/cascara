import Api from '@/services/api';
import { getCategoryQuirks } from '@/services/category.service';
import { getGroups } from '@/services/group.service';
import { getPermissions } from '@/services/permission.service';
import { getTasks } from '@/services/task.service';

const DEFAULT_ITEM_LIMIT = 10;

const getUser = (username: string) => {
  const cleanUsername = encodeURIComponent(username);
  return Api.get(`/users/${cleanUsername}`);
};

const getUserAssignedTasks = (username: string, payload: any) => {
  const params = {
    ...payload,
    candidatesUsernameEq: username,
  };

  return getTasks(params);
};

const getUserSolvedTasks = (username: string, payload: any) => {
  const params = {
    ...payload,
    actorsUsernameEq: username,
  };

  return getTasks(params);
};

const getUserGroups = (username: string, payload: any) => {
  const params = {
    ...payload,
    usersUsernameEq: username,
  };

  return getGroups(params);
};

const getUserPermissions = (username: string, payload: any) => {
  const params = {
    ...payload,
    usersUsernameEq: username,
  };

  return getPermissions(params);
};

const getUserQuirks = (username: string, codename: string, payload: any) => {
  const cleanCodename = encodeURIComponent(codename);
  const params = {
    ...payload,
    usersUsernameEq: username,
  };

  return getCategoryQuirks(cleanCodename, params);
};

const getUsers = ({
  groupsCodenameEq = null,
  limit = DEFAULT_ITEM_LIMIT,
  offset = null,
  searchQuery = null,
} = {}) => {
  const params = {
    limit,
    offset,
    ...((groupsCodenameEq && {
      // eslint-disable-next-line camelcase
      groups__codename__eq: groupsCodenameEq,
    }) as unknown as object),
    ...((searchQuery && {
      // eslint-disable-next-line camelcase
      search_query: searchQuery,
    }) as unknown as object),
  };

  return Api.get(`/users`, { params });
};

const putUser = (username: string, payload: any) => {
  const cleanUsername = encodeURIComponent(username);
  return Api.put(`/users/${cleanUsername}`, {
    email: payload.email,
    /* eslint-disable camelcase */
    fullname: payload.fullname,
    is_active: payload.isActive,
    is_admin: payload.isAdmin,
    is_staff: payload.isStaff,
    is_superuser: payload.isSuperuser,
    /* eslint-disable camelcase */
  });
};

const putUserGroups = (username: string, payload: any) => {
  const cleanUsername = encodeURIComponent(username);
  return Api.put(
    `/users/${cleanUsername}/groups`,
    payload.map((item) => ({ codename: item.codename })),
  );
};

const putUserPermissions = (username: string, payload: any) => {
  const cleanUsername = encodeURIComponent(username);
  return Api.put(
    `/users/${cleanUsername}/permissions`,
    payload.map((item) => ({ codename: item.codename })),
  );
};

export {
  getUser,
  getUserAssignedTasks,
  getUserGroups,
  getUserQuirks,
  getUserPermissions,
  getUserSolvedTasks,
  getUsers,
  putUser,
  putUserGroups,
  putUserPermissions,
};
