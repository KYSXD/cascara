import Api from '@/services/api';

const DEFAULT_ITEM_LIMIT = 10;

const getTask = (id: string) => {
  const cleanId = encodeURIComponent(id);
  return Api.get(`/pointers/${cleanId}`);
};

const getTaskForms = (id: string) => {
  const cleanId = encodeURIComponent(id);
  return Api.get(`/pointers/${cleanId}/forms`);
};

/* eslint complexity: ["error", 27] */
// eslint-disable-next-line max-lines-per-function
const getTasks = ({
  limit = DEFAULT_ITEM_LIMIT,
  actorsUsernameEq = null,
  actorsGroupsCodenameEq = null,
  actorsGroupsCodenameIn = null,
  candidatesUsernameEq = null,
  candidatesGroupsCodenameEq = null,
  candidatesGroupsCodenameIn = null,
  searchQuery = null,
  startedAtDateGe = null,
  startedAtDateLe = null,
  startedAtGt = null,
  startedAtLt = null,
  statusIn = null,
  statusEq = null,
} = {}) => {
  /* eslint-disable camelcase */
  const params = {
    limit,
    ...((actorsUsernameEq && { actors__identifier__eq: actorsUsernameEq }) ?? {}),
    ...((actorsGroupsCodenameEq && { actors__groups__codename__eq: actorsGroupsCodenameEq }) ?? {}),
    ...((actorsGroupsCodenameIn && { actors__groups__codename__in: actorsGroupsCodenameIn }) ?? {}),
    ...((candidatesUsernameEq && { candidates__identifier__eq: candidatesUsernameEq }) ?? {}),
    ...((candidatesGroupsCodenameEq && {
      candidates__groups__codename__eq: candidatesGroupsCodenameEq,
    }) ??
      {}),
    ...((candidatesGroupsCodenameIn && {
      candidates__groups__codename__in: candidatesGroupsCodenameIn,
    }) ??
      {}),
    ...((searchQuery && { search_query: searchQuery }) ?? {}),
    ...((startedAtDateGe && { started_at__date__ge: startedAtDateGe }) ?? {}),
    ...((startedAtDateLe && { started_at__date__le: startedAtDateLe }) ?? {}),
    ...((startedAtGt && { started_at__gt: startedAtGt }) ?? {}),
    ...((startedAtLt && { started_at__lt: startedAtLt }) ?? {}),
    ...((statusIn && { status__in: String(statusIn) }) ?? {}),
    ...((statusEq && { status__eq: statusEq }) ?? {}),
  };
  /* eslint-disable camelcase */

  return Api.get(`/pointers`, {
    params,
  });
};

export { getTask, getTaskForms, getTasks };
