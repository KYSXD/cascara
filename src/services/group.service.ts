import Api from '@/services/api';
import { getPermissions } from '@/services/permission.service';
import { getTasks } from '@/services/task.service';
import { getUsers } from '@/services/user.service';

const DEFAULT_ITEM_LIMIT = 10;

const getGroup = (codename: string) => {
  const cleanCodename = encodeURIComponent(codename);
  return Api.get(`/groups/${cleanCodename}`);
};

const getGroupUsers = (codename: string, payload: any) => {
  const params = {
    ...payload,
    groupsCodenameEq: codename,
  };

  return getUsers(params);
};

const getGroupPermissions = (codename: string, payload: any) => {
  const params = {
    ...payload,
    groupsCodenameEq: codename,
  };

  return getPermissions(params);
};

// eslint-disable-next-line max-lines-per-function
const getGroupAssignedTasks = (codename: string, payload: any) => {
  const params = {
    ...payload,
    candidatesGroupsCodenameEq: codename,
  };

  return getTasks(params);
};

// eslint-disable-next-line max-lines-per-function
const getGroupSolvedTasks = (codename: string, payload: any) => {
  const params = {
    ...payload,
    actorsGroupsCodenameEq: codename,
  };

  return getTasks(params);
};

const getGroups = ({
  limit = DEFAULT_ITEM_LIMIT,
  offset = null,
  searchQuery = null,
  usersUsernameEq = null,
} = {}) => {
  const params = {
    limit,
    offset,
    ...((searchQuery && {
      // eslint-disable-next-line camelcase
      search_query: searchQuery,
    }) as unknown as object),
    ...((usersUsernameEq && {
      // eslint-disable-next-line camelcase
      users__identifier__eq: String(usersUsernameEq),
    }) as unknown as object),
  };

  return Api.get(`/groups`, { params });
};

const postGroup = (payload: any) => {
  return Api.post(`/groups`, {
    codename: payload.codename,
    name: payload.name,
    quirks: [],
  });
};

export {
  getGroup,
  getGroupAssignedTasks,
  getGroupPermissions,
  getGroupSolvedTasks,
  getGroupUsers,
  getGroups,
  postGroup,
};
