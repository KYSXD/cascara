import Api from '@/services/api';

const postLogin = (username: string, password: string, provider: string) => {
  return Api.post(`/auth/signin/${provider}`, {
    password,
    username,
  });
};

const getProviders = () => {
  return Api.get('/auth/providers');
};

export { getProviders, postLogin };
