import Api from '@/services/api';

const DEFAULT_ITEM_LIMIT = 10;

const getCategory = (codename: string) => {
  const cleanCodename = encodeURIComponent(codename);
  return Api.get(`/categories/${cleanCodename}`);
};

const getCategoryQuirks = (
  codename: string,
  { limit = DEFAULT_ITEM_LIMIT, offset = null, usersUsernameEq = null, searchQuery = null } = {},
) => {
  const cleanCodename = encodeURIComponent(codename);
  const params = {
    limit,
    offset,
    ...((searchQuery && {
      // eslint-disable-next-line camelcase
      search_query: searchQuery,
    }) as unknown as object),
    ...((usersUsernameEq && {
      // eslint-disable-next-line camelcase
      users__identifier__eq: usersUsernameEq,
    }) as unknown as object),
  };

  return Api.get(`/categories/${cleanCodename}/quirks`, {
    params,
  });
};

const getCategories = ({ limit = DEFAULT_ITEM_LIMIT, offset = null, searchQuery = null } = {}) => {
  const params = {
    limit,
    offset,
    ...((searchQuery && {
      // eslint-disable-next-line camelcase
      search_query: searchQuery,
    }) as unknown as object),
  };

  return Api.get(`/categories`, { params });
};

const postCategory = (payload: any) => {
  return Api.post(`/categories`, {
    codename: payload.codename,
    name: payload.name,
    quirks: [],
  });
};

const postCategoryQuirk = (codename: string, payload: any) => {
  const cleanCodename = encodeURIComponent(codename);

  return Api.post(`/categories/${cleanCodename}/quirks`, {
    codename: payload.codename,
    name: payload.name,
  });
};

export { getCategory, getCategoryQuirks, getCategories, postCategory, postCategoryQuirk };
