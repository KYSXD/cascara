import Api from '@/services/api';

const DEFAULT_ITEM_LIMIT = 10;

const getExecution = (id: string) => {
  const cleanId = encodeURIComponent(id);
  return Api.get(`/executions/${cleanId}`);
};

const getExecutionSummary = (id: string) => {
  const cleanId = encodeURIComponent(id);
  return Api.get(`/execution/${cleanId}/summary`);
};

const getExecutionTasks = (id: string, payload: any) => {
  const params = {
    ...payload,
    // eslint-disable-next-line camelcase
    execution__id__eq: id,
  };

  return Api.get(`/pointers`, {
    params,
  });
};

const getExecutions = ({
  limit = DEFAULT_ITEM_LIMIT,
  searchQuery = null,
  startedAtDateGe = null,
  startedAtDateLe = null,
  startedAtGt = null,
  startedAtLt = null,
  statusIn = null,
  statusEq = null,
  tasksActorsGroupsCodenameEq = null,
} = {}) => {
  /* eslint-disable camelcase */
  const params = {
    limit,
    ...((searchQuery && { search_query: searchQuery }) ?? {}),
    ...((startedAtDateGe && { started_at__date__ge: startedAtDateGe }) ?? {}),
    ...((startedAtDateLe && { started_at__date__le: startedAtDateLe }) ?? {}),
    ...((startedAtGt && { started_at__gt: startedAtGt }) ?? {}),
    ...((startedAtLt && { started_at__lt: startedAtLt }) ?? {}),
    ...((statusIn && { status__in: String(statusIn) }) ?? {}),
    ...((statusEq && { status__eq: statusEq }) ?? {}),
    ...((tasksActorsGroupsCodenameEq && {
      tasks__actors__groups__codename__eq: tasksActorsGroupsCodenameEq,
    }) ??
      {}),
  };
  /* eslint-enable camelcase */

  return Api.get(`/executions`, {
    params,
  });
};

export { getExecution, getExecutionSummary, getExecutionTasks, getExecutions };
