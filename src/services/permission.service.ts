import Api from '@/services/api';

const DEFAULT_ITEM_LIMIT = 10;

const getPermissions = ({
  limit = DEFAULT_ITEM_LIMIT,
  offset = null,
  searchQuery = null,
  groupsCodenameEq = null,
  groupsCodenameIn = null,
  usersUsernameEq = null,
  usersUsernameIn = null,
} = {}) => {
  /* eslint-disable camelcase */
  const params = {
    limit,
    offset,
    ...((searchQuery && { search_query: searchQuery }) ?? {}),
    ...((groupsCodenameEq && { groups__codename__eq: groupsCodenameEq }) ?? {}),
    ...((groupsCodenameIn && { groups__codename__in: groupsCodenameIn }) ?? {}),
    ...((usersUsernameEq && { users__identifier__eq: usersUsernameEq }) ?? {}),
    ...((usersUsernameIn && { users__identifier__in: usersUsernameIn }) ?? {}),
  };
  /* eslint-disable camelcase */

  return Api.get(`/permissions`, { params });
};

export { getPermissions };
