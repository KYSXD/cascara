import Permission from '@/models/Permission.model';
import { Repository } from 'pinia-orm';
import { getPermissions } from '@/services/permission.service';
import { getUserPermissions } from '@/services/user.service';

class PermissionRepository extends Repository {
  use = Permission;

  async fetchAll(payload: any) {
    const { data } = await getPermissions(payload);

    return this.save(
      data.items.map((item: any) => ({
        ...item,
        codename: item.codename,
      })),
    );
  }

  async fetchPermissions(payload: any) {
    const { data } = await getPermissions(payload);

    return this.save(
      data.items.map((item: any) => ({
        ...item,
        codename: item.codename,
      })),
    );
  }

  async fetchUserPermissions(username: string, payload: any) {
    const { data } = await getUserPermissions(username, payload);

    return this.save(
      data.items.map((item: any) => ({
        ...item,
        codename: item.codename,
        users: [{ username }],
      })),
    );
  }
}

export { PermissionRepository };
