import {
  getGroup,
  getGroupAssignedTasks,
  getGroupPermissions,
  getGroupSolvedTasks,
  getGroups,
  postGroup,
} from '@/services/group.service';
import Group from '@/models/Group.model';
import { Repository } from 'pinia-orm';
import { getUserGroups } from '@/services/user.service';

class GroupRepository extends Repository {
  use = Group;

  async fetchAll(payload: any) {
    const { data } = await getGroups(payload);

    this.save(
      data.items.map((item: any) => ({
        ...item,
        codename: item.codename,
        userCount: item.user_count,
      })),
    );

    return data.items.map((item: any) => item.codename);
  }

  async fetchGroup(codename: string) {
    const { data } = await getGroup(codename);

    this.save({
      ...data,
      codename: data.codename,
      userCount: data.user_count,
    });
  }

  async fetchUserGroups(username: string, payload: any) {
    const { data } = await getUserGroups(username, payload);

    return this.save(
      data.items.map((item: any) => ({
        ...item,
        codename: item.codename,
        userCount: item.user_count,
        users: [{ username }],
      })),
    );
  }

  async fetchGroupAssignedTasks(codename: string, payload: any) {
    const { data } = await getGroupAssignedTasks(codename, payload);

    this.save({
      assignedTasks: data.items.map((item: any) => ({
        ...item,
        executionId: item.execution_id,
        finishedAt: item.finished_at,
        id: item.id,
        startedAt: item.started_at,
        status: item.status,
      })),
      codename,
    });

    return data.items.map((item: any) => item.id);
  }

  async fetchGroupSolvedTasks(codename: string, payload: any) {
    const { data } = await getGroupSolvedTasks(codename, payload);

    this.save({
      codename,
      solvedTasks: data.items.map((item: any) => ({
        ...item,
        executionId: item.execution_id,
        finishedAt: item.finished_at,
        id: item.id,
        startedAt: item.started_at,
        status: item.status,
      })),
    });

    return data.items.map((item: any) => item.id);
  }

  async fetchGroupPermissions(codename: string, payload: any) {
    const { data } = await getGroupPermissions(codename, payload);

    this.save({
      codename,
      permissions: data.items.map((item: any) => ({
        ...item,
        codename: item.codename,
      })),
    });

    return data.items.map((item: any) => item.codename);
  }

  // eslint-disable-next-line class-methods-use-this
  async pushGroup(payload: any) {
    await postGroup(payload);
  }
}

export { GroupRepository };
