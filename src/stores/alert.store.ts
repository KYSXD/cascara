import { defineStore } from 'pinia';

const useAlertStore = defineStore({
  actions: {
    clear() {
      this.alert = null;
    },
    error(message: string) {
      this.alert = { message, type: 'alert-danger' };
    },
    success(message: string) {
      this.alert = { message, type: 'alert-success' };
    },
  },

  id: 'alert',

  state: () => ({
    alert: null,
  }),
});

export { useAlertStore };
