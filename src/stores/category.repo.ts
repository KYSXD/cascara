import {
  getCategories,
  getCategory,
  getCategoryQuirks,
  postCategory,
  postCategoryQuirk,
} from '@/services/category.service';
import Category from '@/models/Category.model';
import { Repository } from 'pinia-orm';

class CategoryRepository extends Repository {
  use = Category;

  async fetchAll(payload: any) {
    const { data } = await getCategories(payload);

    this.save(
      data.items.map((item: any) => ({
        ...item,
        codename: item.codename,
      })),
    );

    return data.items.map((item: any) => item.codename);
  }

  async fetchCategory(codename: string) {
    const { data } = await getCategory(codename);

    this.save({
      ...data,
      codename: data.codename,
    });

    return data.codename;
  }

  async fetchCategoryQuirks(codename: string, payload: any) {
    const { data } = await getCategoryQuirks(codename, payload);

    this.save({
      codename,
      quirks: data.items.map((item: any) => ({
        ...item,
        categoryCodename: codename,
        codename: item.codename,
        name: item.name,
        pk: `${codename}.${item.codename}`,
      })),
    });

    return data.items.map((item: any) => `${codename}.${item.codename}`);
  }

  // eslint-disable-next-line class-methods-use-this
  async pushCategory(payload: any) {
    await postCategory(payload);
  }

  // eslint-disable-next-line class-methods-use-this
  async pushCategoryQuirk(codename, payload) {
    await postCategoryQuirk(codename, payload);
  }
}

export { CategoryRepository };
