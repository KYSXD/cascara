import { getExecution, getExecutionSummary, getExecutions } from '@/services/execution.service';
import Execution from '@/models/Execution.model';
import { Repository } from 'pinia-orm';

class ExecutionRepository extends Repository {
  use = Execution;

  // eslint-disable-next-line max-statements
  async fetchExecutions(payload: object) {
    const { data } = await getExecutions(payload);

    return this.save(
      data.items.map((item: any) => ({
        ...item,
        finishedAt: item.finished_at,
        id: item.id,
        startedAt: item.started_at,
        status: item.status,
      })),
    );
  }

  async fetchExecution(id: string) {
    const { data } = await getExecution(id);

    return this.save({
      ...data,
      finishedAt: data.finished_at,
      id: data.id,
      startedAt: data.started_at,
      status: data.status,
    });
  }

  async fetchExecutionSummary(id) {
    const { data } = await getExecutionSummary(id);

    this.save({
      id,
      summary: data,
    });
  }
}

export { ExecutionRepository };
