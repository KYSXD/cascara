import {
  getUser,
  getUsers,
  putUser,
  putUserGroups,
  putUserPermissions,
} from '@/services/user.service';
import { Repository } from 'pinia-orm';
import User from '@/models/User.model';
import { getGroupUsers } from '@/services/group.service';

class UserRepository extends Repository {
  use = User;

  async fetchUsers(payload: object) {
    const { data } = await getUsers(payload);

    return this.save(
      data.items.map((item: any) => ({
        ...item,
        isActive: item.is_active,
        isAdmin: item.is_admin,
        isStaff: item.is_staff,
        isSuperuser: item.is_superuser,
        pendingTaskCount: item.pending_task_count,
        username: item.identifier,
      })),
    );
  }

  async fetchUser(username: string) {
    const { data } = await getUser(username);

    return this.save({
      ...data,
      isActive: data.is_active,
      isAdmin: data.is_admin,
      isStaff: data.is_staff,
      isSuperuser: data.is_superuser,
      pendingTaskCount: data.pending_task_count,
      username: data.identifier,
    });
  }

  async fetchGroupUsers(codename: string, payload: any) {
    const { data } = await getGroupUsers(codename, payload);

    return this.save(
      data.items.map((item: any) => ({
        ...item,
        groups: [{ codename }],
        isActive: item.is_active,
        isAdmin: item.is_admin,
        isStaff: item.is_staff,
        isSuperuser: item.is_superuser,
        pendingTaskCount: item.pending_task_count,
        username: item.identifier,
      })),
    );
  }

  // eslint-disable-next-line class-methods-use-this
  async setUser(username: string, payload: any) {
    await putUser(username, payload);
  }

  // eslint-disable-next-line class-methods-use-this
  async setUserGroups(username: string, payload: any) {
    await putUserGroups(username, payload);
  }

  // eslint-disable-next-line class-methods-use-this
  async setUserPermissions(username: string, payload: any) {
    await putUserPermissions(username, payload);
  }
}

export { UserRepository };
