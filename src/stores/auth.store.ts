import { getProviders, postLogin } from '@/services/auth.service';
import { defineStore } from 'pinia';
import router from '@/router';
import { useAlertStore } from '@/stores/alert.store';

const useAuthStore = defineStore({
  actions: {
    async fetchProviders() {
      const { data } = await getProviders();

      this.providers = data.items.map((item: any) => ({
        label: item.label,
        value: item.key,
      }));
    },

    async login(username: string, password: string, provider: string) {
      try {
        const { data } = await postLogin(username, password, provider);

        this.user = {
          fullname: data.data.fullname,
          token: data.data.token,
          username: data.data.identifier,
        };

        localStorage.setItem(
          'user',
          JSON.stringify({
            fullname: data.data.fullname,
            token: data.data.token,
            username: data.data.identifier,
          }),
        );

        router.push(this.returnUrl || '/');
      } catch (error) {
        const message = 'validation.invalid';
        const statusCodeUnauthorized = 401;

        if (
          error.response?.status === statusCodeUnauthorized &&
          Array.isArray(error.response.data?.errors)
        ) {
          const data = error.response.data.errors.find((err) => err.code);
          useAlertStore().error(data?.code || message);
        }
      }
    },

    logout() {
      this.user = null;
      localStorage.removeItem('user');
      router.push('/account/login');
    },
  },

  id: 'auth',

  state: () => ({
    providers: [],
    returnUrl: null,
    user: JSON.parse(localStorage.getItem('user') || 'null'),
  }),
});

export { useAuthStore };
