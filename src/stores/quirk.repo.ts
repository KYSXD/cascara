import Quirk from '@/models/Quirk.model';
import { Repository } from 'pinia-orm';
import { getUserQuirks } from '@/services/user.service';

class QuirkRepository extends Repository {
  use = Quirk;

  async fetchUserQuirks(username: string, categoryCodename: string, payload: any) {
    const { data } = await getUserQuirks(username, categoryCodename, payload);

    return this.save(
      data.items.map((item: any) => ({
        ...item,
        categoryCodename,
        codename: item.codename,
        name: item.name,
        pk: `${categoryCodename}.${item.codename}`,
        users: [{ username }],
      })),
    );
  }
}

export { QuirkRepository };
