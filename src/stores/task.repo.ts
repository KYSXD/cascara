import { getTask, getTasks } from '@/services/task.service';
import { getUserAssignedTasks, getUserSolvedTasks } from '@/services/user.service';
import { Repository } from 'pinia-orm';
import Task from '@/models/Task.model';
import { getExecutionTasks } from '@/services/execution.service';

class TaskRepository extends Repository {
  use = Task;

  async fetchTasks(payload: object) {
    const { data } = await getTasks(payload);

    return this.save(
      data.items.map((item: any) => ({
        ...item,
        executionId: item.execution_id,
        finishedAt: item.finished_at,
        id: item.id,
        startedAt: item.started_at,
        status: item.status,
      })),
    );
  }

  async fetchTask(id: string) {
    const { data } = await getTask(id);

    this.save({
      ...data,
      finishedAt: data.finished_at,
      id: data.id,
      startedAt: data.started_at,
      status: data.status,
    });
  }

  async fetchUserAssignedTasks(username: string, payload: any) {
    const { data } = await getUserAssignedTasks(username, payload);

    return this.save(
      data.items.map((item: any) => ({
        ...item,
        candidates: [{ username }],
        executionId: item.execution_id,
        finishedAt: item.finished_at,
        id: item.id,
        startedAt: item.started_at,
        status: item.status,
      })),
    );
  }

  async fetchUserSolvedTasks(username: string, payload: any) {
    const { data } = await getUserSolvedTasks(username, payload);

    return this.save(
      data.items.map((item: any) => ({
        ...item,
        actors: [{ username }],
        executionId: item.execution_id,
        finishedAt: item.finished_at,
        id: item.id,
        startedAt: item.started_at,
        status: item.status,
      })),
    );
  }

  async fetchExecutionTasks(id: string, payload: any) {
    const { data } = await getExecutionTasks(id, payload);

    return this.save(
      data.items.map((item) => ({
        ...item,
        executionId: item.execution_id,
        finishedAt: item.finished_at,
        id: item.id,
        startedAt: item.started_at,
        status: item.status,
      })),
    );
  }
}

export { TaskRepository };
