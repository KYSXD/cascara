import { Model } from 'pinia-orm';

export default class GroupPermission extends Model {
  static entity = 'groupPermissions';

  static primaryKey = ['groupCodename', 'permissionCodename'];

  static fields() {
    return {
      groupCodename: this.string(''),
      permissionCodename: this.string(''),
    };
  }
}
