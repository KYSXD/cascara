import { Model } from 'pinia-orm';

export default class GroupUser extends Model {
  static entity = 'groupUsers';

  static primaryKey = ['groupCodename', 'userUsername'];

  static fields() {
    return {
      groupCodename: this.string(''),
      userUsername: this.string(''),
    };
  }
}
