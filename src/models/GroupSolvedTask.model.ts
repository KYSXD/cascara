import { Model } from 'pinia-orm';

export default class GroupSolvedTask extends Model {
  static entity = 'groupSolvedTasks';

  static primaryKey = ['groupCodename', 'taskId'];

  static fields() {
    return {
      groupCodename: this.string(''),
      taskId: this.string(''),
    };
  }
}
