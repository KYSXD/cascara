import { Model } from 'pinia-orm';

export default class GroupAssignedTask extends Model {
  static entity = 'groupAssignedTasks';

  static primaryKey = ['groupCodename', 'taskId'];

  static fields() {
    return {
      groupCodename: this.string(''),
      taskId: this.string(''),
    };
  }
}
