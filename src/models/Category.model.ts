import { Model } from 'pinia-orm';
import Quirk from '@/models/Quirk.model';

export default class Category extends Model {
  static entity = 'categories';

  static primaryKey = 'codename';

  static fields() {
    return {
      codename: this.string(''),
      name: this.string(''),
      quirks: this.hasMany(Quirk, 'categoryCodename'),
    };
  }
}
