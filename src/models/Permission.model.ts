import Group from '@/models/Group.model';
import GroupPermission from '@/models/GroupPermission.model';
import { Model } from 'pinia-orm';
import User from '@/models/User.model';
import UserPermission from '@/models/UserPermission.model';

export default class Permission extends Model {
  static entity = 'permission';

  static primaryKey = 'codename';

  static fields() {
    return {
      codename: this.string(''),
      groups: this.belongsToMany(Group, GroupPermission, 'permissionCodename', 'groupCodename'),
      name: this.string(''),
      users: this.belongsToMany(User, UserPermission, 'permissionCodename', 'userUsername'),
    };
  }
}
