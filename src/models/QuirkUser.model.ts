import { Model } from 'pinia-orm';

export default class QuirkUser extends Model {
  static entity = 'quirkUsers';

  static primaryKey = ['quirkPk', 'userUsername'];

  static fields() {
    return {
      quirkPk: this.string(''),
      userUsername: this.string(''),
    };
  }
}
