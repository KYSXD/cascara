import GroupAssignedTask from '@/models/GroupAssignedTask.model';
import GroupPermission from '@/models/GroupPermission.model';
import GroupSolvedTask from '@/models/GroupSolvedTask.model';
import GroupUser from '@/models/GroupUser.model';
import { Model } from 'pinia-orm';
import Permission from '@/models/Permission.model';
import Task from '@/models/Task.model';
import User from '@/models/User.model';

export default class Group extends Model {
  static entity = 'groups';

  static primaryKey = 'codename';

  static fields() {
    return {
      assignedTasks: this.belongsToMany(Task, GroupAssignedTask, 'groupCodename', 'taskId'),
      codename: this.string(''),
      name: this.string(''),
      permissions: this.belongsToMany(
        Permission,
        GroupPermission,
        'groupCodename',
        'permissionCodename',
      ),
      solvedTasks: this.belongsToMany(Task, GroupSolvedTask, 'groupCodename', 'taskId'),
      userCount: this.number(),
      users: this.belongsToMany(User, GroupUser, 'groupCodename', 'userUsername'),
    };
  }
}
