import Group from '@/models/Group.model';
import GroupUser from '@/models/GroupUser.model';
import { Model } from 'pinia-orm';
import Permission from '@/models/Permission.model';
import Quirk from '@/models/Quirk.model';
import QuirkUser from '@/models/QuirkUser.model';
import Task from '@/models/Task.model';
import UserAssignedTask from '@/models/UserAssignedTask.model';
import UserPermission from '@/models/UserPermission.model';
import UserSolvedTask from '@/models/UserSolvedTask.model';

export default class User extends Model {
  static entity = 'users';

  static primaryKey = 'username';

  static fields() {
    return {
      assignedTasks: this.belongsToMany(Task, UserAssignedTask, 'userUsername', 'taskId'),
      email: this.string(''),
      fullname: this.string(''),
      groups: this.belongsToMany(Group, GroupUser, 'userUsername', 'groupCodename'),
      isActive: this.boolean(false),
      isAdmin: this.boolean(false),
      isStaff: this.boolean(false),
      isSuperuser: this.boolean(false),
      pendingTaskCount: this.number(),
      permissions: this.belongsToMany(
        Permission,
        UserPermission,
        'userUsername',
        'permissionCodename',
      ),
      quirks: this.belongsToMany(Quirk, QuirkUser, 'userUsername', 'quirkPk', 'username', 'pk'),
      solvedTasks: this.belongsToMany(Task, UserSolvedTask, 'userUsername', 'taskId'),
      username: this.string(''),
    };
  }
}
