import { Model } from 'pinia-orm';

export default class UserSolvedTask extends Model {
  static entity = 'userSolvedTasks';

  static primaryKey = ['userUsername', 'taskId'];

  static fields() {
    return {
      taskId: this.string(''),
      userUsername: this.string(''),
    };
  }
}
