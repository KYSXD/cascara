import Category from '@/models/Category.model';
import { Model } from 'pinia-orm';
import QuirkUser from '@/models/QuirkUser.model';
import User from '@/models/User.model';

export default class Quirk extends Model {
  static entity = 'quirks';

  static primaryKey = ['categoryCodename', 'codename'];

  static fields() {
    return {
      category: this.belongsTo(Category, 'categoryCodename', 'codename'),
      categoryCodename: this.string(''),
      codename: this.string(''),
      name: this.string(''),
      pk: this.string(''),
      users: this.belongsToMany(User, QuirkUser, 'quirkPk', 'userUsername', 'pk', 'username'),
    };
  }
}
