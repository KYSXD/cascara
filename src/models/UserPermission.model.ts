import { Model } from 'pinia-orm';

export default class UserPermission extends Model {
  static entity = 'userPermissions';

  static primaryKey = ['userUsername', 'permissionCodename'];

  static fields() {
    return {
      permissionCodename: this.string(''),
      userUsername: this.string(''),
    };
  }
}
