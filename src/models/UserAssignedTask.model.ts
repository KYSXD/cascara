import { Model } from 'pinia-orm';

export default class UserAssignedTask extends Model {
  static entity = 'userAssignedTasks';

  static primaryKey = ['userUsername', 'taskId'];

  static fields() {
    return {
      taskId: this.string(''),
      userUsername: this.string(''),
    };
  }
}
