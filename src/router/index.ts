import { createRouter, createWebHistory } from 'vue-router';
import { useAlertStore } from '@/stores/alert.store';
import { useAuthStore } from '@/stores/auth.store';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      name: 'home',
      path: '/',
      redirect: '/executions',
    },
    {
      component: () => import('@/views/ExecutionsDashboard.vue'),
      name: 'executions-dashboard',
      path: '/executions',
    },
    {
      children: [
        {
          name: 'executions-item',
          path: '',
          redirect: {
            name: 'executions-item-tasks',
          },
        },
        {
          component: () => import('@/views/ExecutionsItemTasks.vue'),
          name: 'executions-item-tasks',
          path: 'tasks',
          props: true,
        },
        {
          component: () => import('@/views/ExecutionsItemSummary.vue'),
          name: 'executions-item-summary',
          path: 'summary',
          props: true,
        },
      ],
      component: () => import('@/views/ExecutionsItem.vue'),
      path: '/executions/:id',
      props: true,
    },
    {
      component: () => import('@/views/TasksDashboard.vue'),
      name: 'tasks-dashboard',
      path: '/tasks',
    },
    {
      component: () => import('@/views/TasksItem.vue'),
      name: 'tasks-item',
      path: '/tasks/:id',
      props: true,
    },
    {
      children: [
        {
          name: 'users-dashboard',
          path: '',
          redirect: {
            name: 'users-list',
          },
        },
        {
          component: () => import('@/views/UsersList.vue'),
          name: 'users-list',
          path: '/admin/users/search',
        },
        {
          component: () => import('@/views/UsersAdd.vue'),
          name: 'users-add',
          path: '/admin/users/add',
        },
      ],
      component: () => import('@/views/UsersDashboard.vue'),
      path: '/admin/users',
    },
    {
      children: [
        {
          name: 'users-item',
          path: '',
          redirect: {
            name: 'users-item-assigned-tasks',
          },
        },
        {
          component: () => import('@/views/UsersItemEdit.vue'),
          name: 'users-item-edit',
          path: '/admin/users/:username/edit',
          props: true,
        },
        {
          component: () => import('@/views/UsersItemAssignedTasks.vue'),
          name: 'users-item-assigned-tasks',
          path: '/admin/users/:username/assigned-tasks',
          props: true,
        },
        {
          component: () => import('@/views/UsersItemSolvedTasks.vue'),
          name: 'users-item-solved-tasks',
          path: '/admin/users/:username/solved-tasks',
          props: true,
        },
        {
          component: () => import('@/views/UsersItemGroups.vue'),
          name: 'users-item-groups',
          path: '/admin/users/:username/groups',
          props: true,
        },
        {
          component: () => import('@/views/UsersItemPermissions.vue'),
          name: 'users-item-permissions',
          path: '/admin/users/:username/permissions',
          props: true,
        },
        {
          component: () => import('@/views/UsersItemQuirks.vue'),
          name: 'users-item-quirks',
          path: '/admin/users/:username/:categoryCodename',
          props: true,
        },
      ],
      component: () => import('@/views/UsersItem.vue'),
      path: '/admin/users/:username',
      props: true,
    },
    {
      children: [
        {
          name: 'groups-dashboard',
          path: '',
          redirect: {
            name: 'groups-list',
          },
        },
        {
          component: () => import('@/views/GroupsList.vue'),
          name: 'groups-list',
          path: '/admin/groups/search',
        },
        {
          component: () => import('@/views/GroupsAdd.vue'),
          name: 'groups-add',
          path: '/admin/groups/add',
        },
      ],
      component: () => import('@/views/GroupsDashboard.vue'),
      path: '/admin/groups',
    },
    {
      children: [
        {
          name: 'groups-item',
          path: '',
          redirect: {
            name: 'groups-item-assigned-tasks',
          },
        },
        {
          component: () => import('@/views/GroupsItemAssignedTasks.vue'),
          name: 'groups-item-assigned-tasks',
          path: 'assigned-tasks',
          props: true,
        },
        {
          component: () => import('@/views/GroupsItemSolvedTasks.vue'),
          name: 'groups-item-solved-tasks',
          path: 'solved-tasks',
          props: true,
        },
        {
          component: () => import('@/views/GroupsItemUsers.vue'),
          name: 'groups-item-users',
          path: 'users',
          props: true,
        },
        {
          component: () => import('@/views/GroupsItemPermissions.vue'),
          name: 'groups-item-permissions',
          path: 'permissions',
          props: true,
        },
        {
          component: () => import('@/views/GroupsItemExecutions.vue'),
          name: 'groups-item-executions',
          path: 'executions',
          props: true,
        },
      ],
      component: () => import('@/views/GroupsItem.vue'),
      path: '/admin/groups/:codename',
      props: true,
    },
    {
      children: [
        {
          name: 'categories-dashboard',
          path: '',
          redirect: {
            name: 'categories-list',
          },
        },
        {
          component: () => import('@/views/CategoriesList.vue'),
          name: 'categories-list',
          path: '/admin/categories/search',
        },
        {
          component: () => import('@/views/CategoriesAdd.vue'),
          name: 'categories-add',
          path: '/admin/categories/add',
        },
      ],
      component: () => import('@/views/CategoriesDashboard.vue'),
      path: '/admin/categories',
    },
    {
      children: [
        {
          name: 'categories-item',
          path: '',
          redirect: {
            name: 'categories-item-quirks',
          },
        },
        {
          component: () => import('@/views/CategoriesItemQuirksDashboard.vue'),
          name: 'categories-item-quirks',
          path: 'quirks',
          props: true,
        },
        {
          component: () => import('@/views/CategoriesItemQuirksAdd.vue'),
          name: 'categories-item-quirks-add',
          path: 'quirks/add',
          props: true,
        },
      ],
      component: () => import('@/views/CategoriesItem.vue'),
      path: '/admin/categories/:codename',
      props: true,
    },
    {
      component: () => import('@/views/AccountLogin.vue'),
      name: 'account-login',
      path: '/account/login',
    },
    {
      component: () => import('@/views/PageNotFound.vue'),
      name: 'not-found',
      path: '/not-found',
    },
    {
      path: '/:pathMatch(.*)*',
      redirect: '/not-found',
    },
  ],
});

router.beforeEach((to, from, next) => {
  const alertStore = useAlertStore();
  const authRequired = !['/account/login'].includes(to.path);
  const authStore = useAuthStore();

  alertStore.clear();

  if (authRequired && !authStore.user) {
    authStore.returnUrl = to.fullPath;
    next({
      name: 'account-login',
    });
  } else {
    next();
  }
});

export default router;
