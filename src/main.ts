import '@/assets/vue-multiselect.scss';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  faAngleDown,
  faAngleUp,
  faCaretRight,
  faCircleXmark,
  faDiagramProject,
  faDownload,
  faFileCircleQuestion,
  faMinus,
  faPenToSquare,
  faPencil,
  faPlus,
  faRightToBracket,
  faSearch,
  faSliders,
  faTrashCan,
  faUsers,
} from '@fortawesome/free-solid-svg-icons';
import AccountLoginForm from '@/components/AccountLoginForm.vue';
import App from './App.vue';
import CasAlert from '@/components/SiteAlert.vue';
import CasFooter from '@/components/SiteFooter.vue';
import CasHeader from '@/components/SiteHeader.vue';
import CategoryForm from '@/components/CategoryForm.vue';
import CategoryQuirkForm from '@/components/CategoryQuirkForm.vue';
import CategorySearchForm from '@/components/CategorySearchForm.vue';
import ExecutionSearchForm from '@/components/ExecutionSearchForm.vue';
import ExecutionsListGroupItem from '@/components/ExecutionsListGroupItem.vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import GroupForm from '@/components/GroupForm.vue';
import GroupSearchForm from '@/components/GroupSearchForm.vue';
import PermissionSearchForm from '@/components/PermissionSearchForm.vue';
import QuirkSearchForm from '@/components/QuirkSearchForm.vue';
import TaskForm from '@/components/TaskForm.vue';
import TaskFormCheckboxSelectMultipleInput from '@/components/TaskFormCheckboxSelectMultipleInput.vue';
import TaskFormDateInput from '@/components/TaskFormDateInput.vue';
import TaskFormFileInput from '@/components/TaskFormFileInput.vue';
import TaskFormNumberInput from '@/components/TaskFormNumberInput.vue';
import TaskFormRadioSelectInput from '@/components/TaskFormRadioSelectInput.vue';
import TaskFormSelectInput from '@/components/TaskFormSelectInput.vue';
import TaskFormTextInput from '@/components/TaskFormTextInput.vue';
import TaskSearchForm from '@/components/TaskSearchForm.vue';
import TasksListGroupItem from '@/components/TasksListGroupItem.vue';
import TextClamp from 'vue3-text-clamp';
import UserAddForm from '@/components/UserAddForm.vue';
import UserEditForm from '@/components/UserEditForm.vue';
import UserSearchForm from '@/components/UserSearchForm.vue';
import UsersDashboardListItem from '@/components/UsersDashboardListItem.vue';
import VueMultiselect from 'vue-multiselect';
import { createApp } from 'vue';
import { createI18n } from 'vue-i18n';
import { createORM } from 'pinia-orm';
import { createPinia } from 'pinia';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';
import { library } from '@fortawesome/fontawesome-svg-core';
import messages from '@intlify/unplugin-vue-i18n/messages';
import router from './router';

const app = createApp(App);
const i18n = createI18n({
  availableLocales: ['es', 'en'],
  fallbackLocale: import.meta.env.VITE_SITE_LOCALE_FALLBACK || 'en',
  globalInjection: true,
  legacy: false,
  locale: import.meta.env.VITE_SITE_LOCALE || 'es',
  messages,
});

// Components
app
  .component('AccountLoginForm', AccountLoginForm)
  .component('CasHeader', CasHeader)
  .component('CasFooter', CasFooter)
  .component('CasAlert', CasAlert)
  .component('CategoryForm', CategoryForm)
  .component('CategoryQuirkForm', CategoryQuirkForm)
  .component('CategorySearchForm', CategorySearchForm)
  .component('GroupForm', GroupForm)
  .component('GroupSearchForm', GroupSearchForm)
  .component('PermissionSearchForm', PermissionSearchForm)
  .component('UsersDashboardListItem', UsersDashboardListItem)
  .component('ExecutionSearchForm', ExecutionSearchForm)
  .component('QuirkSearchForm', QuirkSearchForm)
  .component('TaskForm', TaskForm)
  .component('TaskFormCheckboxSelectMultipleInput', TaskFormCheckboxSelectMultipleInput)
  .component('TaskFormDateInput', TaskFormDateInput)
  .component('TaskFormFileInput', TaskFormFileInput)
  .component('TaskFormNumberInput', TaskFormNumberInput)
  .component('TaskFormRadioSelectInput', TaskFormRadioSelectInput)
  .component('TaskFormSelectInput', TaskFormSelectInput)
  .component('TaskFormTextInput', TaskFormTextInput)
  .component('TaskSearchForm', TaskSearchForm)
  .component('TasksListGroupItem', TasksListGroupItem)
  .component('ExecutionsListGroupItem', ExecutionsListGroupItem)
  .component('UserAddForm', UserAddForm)
  .component('UserEditForm', UserEditForm)
  .component('UserSearchForm', UserSearchForm)
  .component('VueMultiselect', VueMultiselect);

app.use(createPinia().use(createORM()));
app.use(router);

library.add(
  faAngleDown,
  faAngleUp,
  faCaretRight,
  faCircleXmark,
  faDiagramProject,
  faDownload,
  faFileCircleQuestion,
  faEnvelope,
  faMinus,
  faPenToSquare,
  faPencil,
  faPlus,
  faRightToBracket,
  faSearch,
  faSliders,
  faTrashCan,
  faUsers,
);

app.component('FontAwesomeIcon', FontAwesomeIcon);

app.use(i18n);
app.use(TextClamp);
app.mount('#app');
