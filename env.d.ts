// / <reference types="vite/client" />
interface ImportMetaEnv {
  readonly BASE_URL: string;
  readonly VITE_SITE_TITLE: string;
  readonly VITE_SITE_LOCALE: string;
  readonly VITE_SITE_LOCALE_FALLBACK: string;
  readonly VITE_PVM_API_URL: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
