/* eslint-env node */
module.exports = {
  extends: [
    'plugin:vue/vue3-recommended',
    'eslint:all',
    '@vue/eslint-config-typescript',
    '@vue/eslint-config-prettier',
  ],
  parserOptions: {
    ecmaVersion: 'latest',
  },
  root: true,
  rules: {
    'new-cap': 'off',
    'one-var': ['error', 'never'],
  },
};
