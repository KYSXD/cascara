FROM node:20-alpine as dist

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

ARG SITE_TITLE
ARG PVM_API_URL

RUN touch .env \
  && echo VITE_SITE_TITLE="$SITE_TITLE" >> .env \
  && echo VITE_PVM_API_URL="$PVM_API_URL" >> .env

RUN npm run build-only

FROM nginx:1.17

COPY --from=dist /app/dist /usr/share/nginx/html

COPY ./nginx.conf /etc/nginx/nginx.conf
